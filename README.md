# @semantic-release/config-gitlab-poetry

> [**semantic-release**](https://github.com/semantic-release/semantic-release)
> config to publish a [Poetry](https://python-poetry.org) package.

## Getting Started

Add the following to `.releaserc.yaml`:

```yaml
extends:
  - "@semantic-release/config-gitlab-poetry"
```

Combine that with the semantic-release/config-release-channels:

```yaml
extends:
  - "@semantic-release/config-release-channels"
  - "@semantic-release/config-gitlab-poetry"
```

## Configuration

### Authentication

#### GitLab

Create a project access token with `api`/`write_repository` permissions.

Add the token as a `GITLAB_TOKEN` CI protected/masked variable.

#### Poetry

The authentication is found in the environment variables as described by the [Poetry documentation][auth].

Create a [Deploy Token][deploy-token] with `write_repository` permission.

Add the deploy token username/password as the following CI protected/masked variables:

- `POETRY_HTTP_BASIC_GITLAB_USERNAME`
- `POETRY_HTTP_BASIC_GITLAB_PASSWORD`

[auth]: https://python-poetry.org/docs/repositories#configuring-credentials
[deploy-token]: https://docs.gitlab.com/ee/user/project/deploy_tokens/
